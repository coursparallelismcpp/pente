#pragma once
#include <array>
#include <stdexcept>
#include <iomanip>
#include "types.h"

/// stores a grid and implements the functions to interacts with the players
class Board
{
public:
    Grid grid; // grid with the stones
    Stone currentPlayer; // who is the current player
    std::array<unsigned int, 3> nbCaptures; // nbCaptures[currentPlayer] should countain the number of captures of the player
    bool proPenteRule; // are we using ProPente rules ? (you can ignore this)
    unsigned int nbStones; // how many stones have been placed on the board so far

    /// creates a grid with a single Player1 stone in the middle
    Board(const bool useProPenteRule = false): grid(), nbCaptures(), proPenteRule(useProPenteRule)
    {
        // one starts with a stone in the middle
        grid[gridSize/2][gridSize/2] = Player1;
        currentPlayer = Player2;
        nbStones = 1;
    }

    /// adds a stone to the board
    Status addStone(const Move& move)
    {
        // TODO
        throw std::runtime_error("TODO");
    }
};

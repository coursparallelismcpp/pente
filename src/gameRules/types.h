#pragma once

/// All possible type of stone
enum Stone {Empty, Player1, Player2};

/// returns a player of color opposed to the current player
Stone changePlayer(const Stone player)
{
    switch(player)
    {
        case Player1 : return Player2;
        case Player2 : return Player1;
        default : return Empty;
    }
}

/// The size of a Pente grid
const int gridSize = 19;

/// A 19x19 array of stones
using Grid = std::array< std::array<Stone,gridSize> ,gridSize>;

/// Represents a move
struct Move
{
    int row;
    int col;

    /// creates a Move
    Move(const int r, const int c): row(r), col(c) {};
};

/// All possible outcome for a move
enum Status {ValidMove, Win, IllegalMove};

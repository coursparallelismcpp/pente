#pragma once
#include <boost/asio.hpp>
#include "serialize.h"

using boost::asio::ip::tcp;

/// deals with communication via sockets
namespace Com
{
    /// sends a T to the socket
    template<typename T>
    void toSocket(tcp::socket& socket, const T& x)
    {
        // information to be sent
        const Serialize::Buffer<T> buffer = Serialize::toBuffer(x);

        // sends the information with the socket
        boost::system::error_code error;
        boost::asio::write(socket, boost::asio::buffer(buffer), error);

        // failed with some error
        if (error) throw boost::system::system_error(error);
    }

    /// gets a T from the socket
    template<typename T>
    T fromSocket(tcp::socket& socket)
    {
        // reads the information stored in the socket
        Serialize::Buffer<T> buffer;
        boost::system::error_code error;
        //size_t len = socket.read_some(boost::asio::buffer(buffer), error);
        boost::asio::read(socket, boost::asio::buffer(buffer), error);

        // failed with some error
        if (error) throw boost::system::system_error(error);

        // reconstruct the T
        return Serialize::fromBuffer(buffer);
    }
}


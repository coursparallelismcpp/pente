#pragma once
#include "../gameRules/types.h"
#include "../gameRules/board.h"

namespace Serialize
{
    /// returns the size needed to serialize a type
    template<typename T> constexpr int serialized_size() { return -1; }

    /// a buffer built on top of a type T
    /// a buffer is an array of int of size serialized_size<T>()
    template<typename T>
    using Buffer = std::array<int,serialized_size<T>()>;

    //---------------------------------------------------------------
    // Move

    // size needed to serialize a Move
    template<> constexpr int serialized_size<Move>() { return 2; }

    /// exports as a buffer
    Buffer<Move> toBuffer(const Move& move)
    {
        return {move.row, move.col};
    }

    /// imports from a buffer
    /// the type is only identified due to the size of the buffer
    Move fromBuffer(const Buffer<Move>& buffer)
    {
        return Move(buffer[0], buffer[1]);
    }

    //---------------------------------------------------------------
    // Board

    // size needed to serialize a Board
    template<> constexpr int serialized_size<Board>() { return 5 + gridSize*gridSize; }

    /// exports as an buffer
    Buffer<Board> toBuffer(const Board& board)
    {
        Buffer<Board> buffer;

        // stores current player
        buffer[0] = static_cast<int>(board.currentPlayer);

        // stores captures
        buffer[1] = board.nbCaptures[Player1];
        buffer[2] = board.nbCaptures[Player2];

        // stores use of the proPente rule
        buffer[3] = static_cast<int>(board.proPenteRule);

        // stores number of move played so far
        buffer[4] = board.nbStones;

        // stores grid
        int bufferIndex = 5;
        for(int row = 0; row < gridSize; row++)
        {
            for(int col = 0; col < gridSize; col++)
            {
                buffer[bufferIndex] = static_cast<int>(board.grid[row][col]);
                bufferIndex++;
            }
        }

        return buffer;
    }

    /// imports from a buffer
    /// the type is only identified due to the size of the buffer
    Board fromBuffer(const Buffer<Board>& buffer)
    {
        Board board;

        // gets current player
        board.currentPlayer = static_cast<Stone>(buffer[0]);

        // gets captures
        board.nbCaptures[Player1] = buffer[1];
        board.nbCaptures[Player2] = buffer[2];

        // gets use of the proPente rule
        board.proPenteRule = static_cast<bool>(buffer[3]);

        // gets number of move played so far
        board.nbStones = buffer[4];

        // gets grid
        int bufferIndex = 5;
        for(int row = 0; row < gridSize; row++)
        {
            for(int col = 0; col < gridSize; col++)
            {
                board.grid[row][col] = static_cast<Stone>(buffer[bufferIndex]);
                bufferIndex++;
            }
        }

        return board;
    }
}

#pragma once
#include "../gameRules/board.h"

/// Interface
/// represents all the operations that should be implemented by an AI
class ArtificialIntelligence
{
public:
    /// takes a grid, analyses it and returns a move
    virtual Move getMove(const Board& board) = 0;
};
